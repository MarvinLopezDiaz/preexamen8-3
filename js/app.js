function cargarAjx(){
    const url = 'https://jsonplaceholder.typicode.com/todos';
    axios
    .get(url)
    .then((res)=>{
        mostrar(res.data)
    }).catch((err)=>{
        console.log("Surgio un error");
    })
    function mostrar(data){
        const res = document.getElementById('respuesta');
        res.innerHTML = "";
        
        for(item of data){
            res.innerHTML += "<div id=tabla>" + "..::"  + item.userId + "::.." + "<br>" + "ID: " + item.id + "<br>" + "Titulo: "  + item.title + "<br>" + "Complemento: " + item.completed + "</div><br>"
        }
    }
}

const res = document.getElementById('btnCargar');

res.addEventListener('click', function(){
    cargarAjx();
})